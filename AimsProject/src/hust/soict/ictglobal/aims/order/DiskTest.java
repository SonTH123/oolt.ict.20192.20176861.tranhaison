package hust.soict.ictglobal.aims.order;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;

public class DiskTest {
	
	public static void main(String[] args) {
		
		Order anOrder = new Order("01/04/2020");
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Avenger", "Action", "Russo", 120, 34.21f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("IT", "Scary", "Unknown", 100, 4.21f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Doraemon", "Animation", "Unknown", 60, 2.11f);
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Fast&Furious", "Action", "Vin Diesel", 132, 10.65f);
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Need for Speed", "Thriller", "Scott Waugh", 145, 9.73f);
		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Step up", "Musician/Sport", "Jon M. Chu", 95, 13.45f);

		anOrder.addMedia(dvd1);
		anOrder.addMedia(dvd2);
		anOrder.addMedia(dvd3);
		anOrder.addMedia(dvd4);
		anOrder.addMedia(dvd5);
		anOrder.addMedia(dvd6);

		anOrder.printOrder();
		
	}
}
