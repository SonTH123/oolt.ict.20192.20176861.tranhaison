package hust.soict.ictglobal.aims.order;

import java.util.ArrayList;
import java.util.List;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;

public class Order {
	
	private String dateOrdered;
	
	private List<Media> itemsOrdered = new ArrayList<Media>();
	
	public static class nbOrders<itemsOrdered> {
		public static final int MAX_LIMITED_ORDERED = 5;
		private static int nbOrder = 0;
		
		public static boolean checkLimitOrders() {
			if (nbOrder > MAX_LIMITED_ORDERED)
				return false;
			else 
				return true;
		}
	}
	
	public Order(String dateOrdered) {
		super();
		this.dateOrdered = dateOrdered;
		Order.nbOrders.nbOrder++;
	}
	
	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	public void addMedia (Media media) {
		itemsOrdered.add(media);
	}
	
	public boolean removeMedia (int id) {
		if (id >= 1 && id <= itemsOrdered.size()) {
			itemsOrdered.remove(id-1);
			return true;
		}
		return false;
	}
	
	public float totalCost() {
		float cost = 0;
		for (int i=0; i<itemsOrdered.size(); i++) {
			cost += itemsOrdered.get(i).getCost();
		}
		return cost;
	}
	
	public Media getItem(int position) {
		return itemsOrdered.get(position);
	}
	
	public int getTotalItem() {
		return itemsOrdered.size();
	}
	
	public int getEachMedia(int mediaType) {
		int books = 0;
		int dvds = 0;
		int cds = 0;
		for (int i=0; i<itemsOrdered.size(); i++) {
			if (itemsOrdered.get(i) instanceof Book)
				books++;
			if (itemsOrdered.get(i) instanceof DigitalVideoDisc)
				dvds++;
			if (itemsOrdered.get(i) instanceof CompactDisc)
				cds++;
		}
		switch (mediaType) {
		case 0:
			return books;
		case 1:
			return dvds;
		case 2:
			return cds;
		}
		return -1;
	}
	
	public int checkMediaType(int position) {
		if (itemsOrdered.get(position) instanceof Book)
			return 0;
		if (itemsOrdered.get(position) instanceof DigitalVideoDisc)
			return 1;
		if (itemsOrdered.get(position) instanceof CompactDisc)
			return 2;
		return -1;
	}
	
	public void printOrder() {
		System.out.println("Date: " + dateOrdered);
		System.out.println("Ordered items: ");
		for (int i=0; i<itemsOrdered.size(); i++) {
			System.out.println("\t" + (i+1) + ". "
							+ itemsOrdered.get(i).getTitle() + " - " 
							+ itemsOrdered.get(i).getCategory() + " - "
							+ itemsOrdered.get(i).getCost() + "$");
		}
		System.out.println("Total cost: " + totalCost() + "$");
		System.out.println("*************************************");
	}
	
	public void printSpecificMedia(int mediaType) {
		int books = getEachMedia(0);
		int dvds = getEachMedia(1);
		int cds = getEachMedia(2);
		int i;
		
		switch (mediaType) {
		case 0:
			for (i=0; i<books; i++) {
				Book book =(Book) itemsOrdered.get(i);
				System.out.println("\tBook " + (i+1) + ": "
						+ book.getTitle() + " - "
						+ book.getCategory() + " - "
						+ book.getAuthors() + " - "
						+ book.getCost() + "$");
			}
			break;
		case 1:
			for (i=books; i<(books+dvds); i++) {
				DigitalVideoDisc dvd = (DigitalVideoDisc) itemsOrdered.get(i);
				System.out.println("\tDVD " + (i+1) + ": "
						+ dvd.getTitle() + " - "
						+ dvd.getCategory() + " - "
						+ dvd.getDirector() + " - "
						+ dvd.getLength() + "' - "
						+ dvd.getCost() + "$");
			}
			break;
		case 2:
			for (i=(books+dvds); i<(books+dvds+cds); i++) {
				CompactDisc cd = (CompactDisc) itemsOrdered.get(i);
				System.out.println("\tCD " + (i+1) + ": "
						+ cd.getTitle() + " - "
						+ cd.getCategory() + " - "
						+ cd.getArtist() + " - "
						+ cd.getLength() + "' - "
						+ cd.getCost() + "$");
			}
			break;
		}
	}
	
}
