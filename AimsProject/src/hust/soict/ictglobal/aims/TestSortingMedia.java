package hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Track;

public class TestSortingMedia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Iterator<DigitalVideoDisc> dvdIterator;
		List<DigitalVideoDisc> collection = new ArrayList<>();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Avenger", "Action", "Russo", 120, 34.21f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("IT", "Scary", "Unknown", 100, 4.21f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Doraemon", "Animation", "Unknown", 60, 2.11f);
		collection.add(dvd1);
		collection.add(dvd2);
		collection.add(dvd3);
		
		System.out.println("The DVDs currently in the order are:");
		dvdIterator = collection.iterator();
		while (dvdIterator.hasNext()) {
			try {
				dvdIterator.next().play();
			} catch (PlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Collections.sort(collection);
		System.out.println("The DVDs after sorted are:");
		dvdIterator = collection.iterator();
		while (dvdIterator.hasNext()) {
			try {
				dvdIterator.next().play();
			} catch (PlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("-----------------------------------------------");
		
		List<CompactDisc> cdList = new ArrayList<>();
		Iterator<CompactDisc> cdIterator;
		Track track1 = new Track("ABC", 2);
		Track track2 = new Track("DEF", 3);
		CompactDisc cd1 = new CompactDisc("2019", "Country", 2.34f, "Eminem");
		CompactDisc cd2 = new CompactDisc("2020", "EDM", 7.12f, "Jack");
		cd1.addTrack(track1);
		cd1.addTrack(track2);
		cd2.addTrack(track2);
		cdList.add(cd1);
		cdList.add(cd2);
		
		System.out.println("The CDs currently in the order are:");
		cdIterator = cdList.iterator();
		while (cdIterator.hasNext()) {
			try {
				cdIterator.next().play();
			} catch (PlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Collections.sort(cdList);
		System.out.println("The CDs after sorted are:");
		cdIterator = cdList.iterator();
		while (cdIterator.hasNext()) {
			try {
				cdIterator.next().play();
			} catch (PlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("-----------------------------------------------");
	}

}
