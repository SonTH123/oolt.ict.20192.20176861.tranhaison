package hust.soict.ictglobal.aims;

import java.lang.Runnable;
import java.lang.Runtime;

public class MemoryDaemon implements Runnable{

	private long memoryUsed = 0;
	
	public MemoryDaemon() {
		// TODO Auto-generated constructor stub
	}
	
	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;
		
		while (true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != memoryUsed) {
				System.out.println("*Memory used = " + used);
				memoryUsed = used;
			}
		}
	}

}
