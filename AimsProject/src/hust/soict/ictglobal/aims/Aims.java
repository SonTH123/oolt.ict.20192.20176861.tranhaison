package hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {

	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Thread thread = new Thread(new MemoryDaemon());
		thread.setDaemon(true);
		thread.start();
		
		List<Order> orderList = new ArrayList<Order>();
		
		int choice;
		do {
			do {
				showMenu();
				System.out.print("Enter your choice: ");
				choice = scan.nextInt();
			} while (choice < 0 || choice > 5);
			
			switch (choice) {
			case 0:
				System.out.println("See you again!");
				break;
			case 1:
				orderList = createNewOrder(orderList);
				break;
			case 2:
				addItem(orderList);
				break;
			case 3:
				deleteItem(orderList);
				break;
			case 4:
				displayItems(orderList);
				break;
			case 5: 
				playDisc(orderList);
				break;
			}
		} while (choice != 0);
		
	}

	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("\t1. Create new order.");
		System.out.println("\t2. Add item to the order.");
		System.out.println("\t3. Delete item by id.");
		System.out.println("\t4. Display the items list of all orders.");
		System.out.println("\t5. Play a CD/DVD.");
		System.out.println("\t0. Exit.");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}

	public static List<Order> createNewOrder(List<Order> orderList) {
		if (orderList.size() >= 5) {
			System.out.println("You cannot add any new order, only 5 orders allowed!\n");
		} else {
			System.out.println("What is the date today? (dd/MM/yyyy)");
			
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(System. in);
			String date = scanner.nextLine();
			
			if (date.isEmpty()) {
				System.out.println("You must fill in the date\n");
			} else {
				Order newOrder = new Order(date);
				orderList.add(newOrder);
				System.out.println("New order added\n");
			}
		}
		return orderList;
	}
	
	public static void printOrderList(List<Order> orderList) {
		System.out.println("\nYOUR ORDER");
		for (int i=0; i<orderList.size(); i++) {
			System.out.println("\t* Order " + (i+1) + ": " + orderList.get(i).getDateOrdered());
		}
		System.out.println("------------------------------------------");
	}
	
	public static int checkExistOrder(List<Order> orderList) {
		if (orderList.size() == 0) {
			System.out.println("*Sorry, but you have nothing! You should create a new order*\n");
			return 0;
		}
		printOrderList(orderList);
		int order_id;
		System.out.println("What order do you want to modify items? (enter order's number)");
		order_id = scan.nextInt();
		if (order_id <= 0 || order_id > orderList.size()) {
			System.out.println("The order does not exist!\n");
			return 0;
		}
		return order_id;
	}
	
	public static void addItem(List<Order> orderList) {
		int order_id = checkExistOrder(orderList);
		if (order_id != 0) {
			Order order = orderList.get(order_id - 1);
			
			System.out.println("How many books do you want to add?");
			int number_of_books = scan.nextInt();
			
			for (int i=1; i<=number_of_books; i++) {
				System.out.println("Enter the information of book " + i + " (Title/Category/Cost/Author): ");
				
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(System. in);
				String book_info = scanner.nextLine();
				
				String[] book_detail = book_info.split("/");
				String title = book_detail[0];
				String category = book_detail[1];
				float cost = Float.parseFloat(book_detail[2]);
				List<String> author = new ArrayList<>();
				author.add(book_detail[3]);
						
				Book newBook = new Book(title, category, cost, author);
				order.addMedia(newBook);
			}
			
			System.out.println("How many DVDs do you want to add?");
			int number_of_dvds = scan.nextInt();
			
			for (int i=1; i<=number_of_dvds; i++) {
				System.out.println("Enter the information of DVD " + i + " (Title/Category/Cost/Director/Length): ");
				
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(System. in);
				String dvd_info = scanner.nextLine();
				
				String[] dvd_detail = dvd_info.split("/");
				String title = dvd_detail[0];
				String category = dvd_detail[1];
				float cost = Float.parseFloat(dvd_detail[2]);
				String director = dvd_detail[3];
				float length = Float.parseFloat(dvd_detail[4]);
					
				DigitalVideoDisc newDVD = new DigitalVideoDisc(title, category, director, length, cost);
				order.addMedia(newDVD);
			}
			
			System.out.println("How many CDs do you want to add?");
			int number_of_cds = scan.nextInt();
			
			for (int i=1; i<=number_of_cds; i++) {
				System.out.println("Enter the information of CD " + i + " (Title/Category/Artist/Cost): ");
				
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(System.in);
				String cd_info = scanner.nextLine();
				
				String[] cd_detail = cd_info.split("/");
				String title = cd_detail[0];
				String category = cd_detail[1];
				String artist = cd_detail[2];
				float cost = Float.parseFloat(cd_detail[3]);
				
				CompactDisc newCD = new CompactDisc(title, category, cost, artist);
				
				System.out.println("How many tracks in this CD?");
				int number_of_tracks = scan.nextInt();
				
				for (int j=1; j<=number_of_tracks; j++) {
					System.out.println("Enter the information of track " + j + " (Title/Length): ");
					String track_info = scanner.nextLine();
					
					String[] track_detail = track_info.split("/");
					String track_title = track_detail[0];
					float track_length = Float.parseFloat(track_detail[1]);
					
					Track newTrack = new Track(track_title, track_length);
					
					newCD.addTrack(newTrack);
				}
				order.addMedia(newCD);
			}
			System.out.println();
		}
	}
	
	public static void deleteItem(List<Order> orderList) {
		int order_id = checkExistOrder(orderList);
		if (order_id != 0) {
			Order order = orderList.get(order_id - 1);
			order.printOrder();
			
			System.out.println("What item do you want to delete? (enter item's number)");
			int id = scan.nextInt();
			
			boolean isDeleted = order.removeMedia(id);
			if (isDeleted == true) {
				System.out.println("The item has been removed from the order!\n");
			} else {
				System.out.println("The item does not exist!\n");
			}
		}
	}
	
	public static void displayItems(List<Order> orderList) {
		System.out.println("\nLIST OF ORDERS\n");
		if (orderList.size() == 0) {
			System.out.println("*Sorry, but you have nothing in your list*\n");
		} else {
			for (int i=0; i<orderList.size(); i++) {
				System.out.println("ORDER " + (i+1));
				orderList.get(i).printOrder();
				System.out.println();
			}
		}
	}
	
	public static void playDisc(List<Order> orderList) {
		int order_id = checkExistOrder(orderList);
		if (order_id != 0) {
			Order order = orderList.get(order_id - 1);
			
			System.out.println("LIST OF DISCS");
			order.printSpecificMedia(1);
			order.printSpecificMedia(2);
			
			System.out.println("What do you want to play? (enter disc's number)");
			int disc = scan.nextInt();
			
			int books = order.getEachMedia(0);
			if (disc > order.getTotalItem() || disc <= books) {
				System.out.println("Disc does not exist!\n");
			} else {
				if (order.checkMediaType(disc - 1) == 1) {
					DigitalVideoDisc dvd = (DigitalVideoDisc) order.getItem(disc - 1);
					try {
						dvd.play();
					} catch (PlayerException e) {
						e.printStackTrace();
					}
				} else if (order.checkMediaType(disc - 1) == 2) {
					CompactDisc cd = (CompactDisc) order.getItem(disc - 1);
					try {
						cd.play();
					} catch (PlayerException e) {
						e.printStackTrace();
					}
				} else
					return;
			}
		}
	}
	
}

