package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<DigitalVideoDisc> {
	
	public DigitalVideoDisc(String title) {
		super(title);
	} 
	
	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}

	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category);
	}

	public DigitalVideoDisc(String title, String category, String director, float length, float cost) {
		super(title, category, cost, length, director);
	}

	public boolean Search (String title) {
		if (this.getTitle().contains(title) == true)
			return true;
		return false;
	}

	@Override
	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERROR: DVD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD's length: " + this.getLength() + "'\n");
	}

	/**
	 * Compare DVDs by cost from lowest to highest
	 */
	@Override
	public int compareTo(DigitalVideoDisc dvd) {
		// TODO Auto-generated method stub
		if (this.getCost() > dvd.getCost())
			return 1;
		if (this.getCost() < dvd.getCost())
			return -1;
		return 0;
	}

}
