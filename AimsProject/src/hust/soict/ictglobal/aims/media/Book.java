package hust.soict.ictglobal.aims.media;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

public class Book extends Media implements Comparable<Book>{
	
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();
	private List<String> authors = new ArrayList<String>();
	
	public Book() {
		super();
	}

	public Book(String title) {
		super(title);
	}

	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}

	public Book(String title, String category, float cost, List<String> authors) {
		super(title, category, cost);
		this.authors = authors;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public boolean addAuthor (String authorName) {
		for (int i=0; i<authors.size(); i++) {
			if (authors.get(i) == authorName)
				return false;
		}
		authors.add(authorName);
		return true;
	}

	public boolean removeAuthor (String authorName) {
		for (int i=0; i<authors.size(); i++) {
			if (authors.get(i) == authorName) {
				authors.remove(i);
				return true;
			}
		}
		return false;
	}

	/**
	 * Compare 2 books by title
	 */
	@Override
	public int compareTo(Book book) {
		// TODO Auto-generated method stub
		return this.getTitle().compareTo(book.getTitle());
	}
	
	public void processContent() {
		String regex = "[!._,;:/'@?& ]";
		StringTokenizer stringToken = new StringTokenizer(content, regex);
		
		// Add all tokens into an Array List
		while (stringToken.hasMoreTokens()) {
			contentTokens.add(stringToken.nextToken());
		}
		
		// Sort the tokens from a-z
		Collections.sort(contentTokens);
		
		// Put all tokens and their frequency into Map 
		for (int i=0; i<contentTokens.size(); i++) {
			String key = contentTokens.get(i);
			if (wordFrequency.containsKey(key)) {
				int count = wordFrequency.get(key);
				wordFrequency.put(key, count+1);
			} else {
				wordFrequency.put(key, 1);
			}
		}
	}
	
	public String toString() {
		String tokenList = "";
		Set<String> set = new TreeSet<>(wordFrequency.keySet());
		for (String key : set) {
			tokenList += "\t" + key + ": " + wordFrequency.get(key) + "\n";
		}
		String bookInfo = "Book's information: " 
				+ this.getTitle() + " - "
				+ this.getAuthors() + " - " 
				+ this.getCategory() + " - " 
				+ this.getCost() + "$"
				+ "\nNumber of tokens: " + contentTokens.size()
				+ "\n" + tokenList;
		
		return bookInfo;
	}
}
