package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hust.soict.ictglobal.aims.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc> {

	private String artist;
	private float length;
	private List<Track> tracks = new ArrayList<Track>();
	
	public CompactDisc(String title, String category, String director) {
		super(title, category, director);
	}
	
	public CompactDisc(String title, String category, float cost, String artist) {
		super(title, category, cost);
		this.artist = artist;
	}
	
	public CompactDisc(String title, String category, float cost, float length, String director) {
		super(title, category, cost, length, director);
	}
	
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public boolean addTrack(Track track) {
		for (int i=0; i<tracks.size(); i++) {
			if (tracks.get(i) == track) {
				return false;
			}
		}
		tracks.add(track);
		return true;
	}

	public boolean removeTrack(Track track) {
		for (int i=0; i<tracks.size(); i++) {
			if (tracks.get(i) == track) {
				tracks.remove(i);
				return true;
			}
		}
		return false;
	}
	
	public int getNumberTracks() {
		return tracks.size();
	}
	
	public float getLength() {
		length = 0;
		for (int i=0; i<tracks.size(); i++) {
			length += tracks.get(i).getLength();
		}
		return length;
	}

	@Override
	public void play() throws PlayerException{
		if (this.getLength() <= 0) {
			System.err.println("ERROR: CD length is 0");
			throw (new PlayerException());
		}
		
		System.out.println("Artist: " + artist);
		System.out.println("Total length of CD: " + this.getLength() + "'");
		
		Iterator<Track> iterator = tracks.iterator();
		Track nextTrack;
		
		while (iterator.hasNext()) {
			nextTrack = (Track) iterator.next();
			try {
				nextTrack.play();
			} catch (PlayerException e) {
				e.printStackTrace();
			}
		}
		/*
		 * for (int i=0; i<tracks.size(); i++) { System.out.println("\tTrack " + (i+1) +
		 * ": " + tracks.get(i).getTitle() + " - " + tracks.get(i).getLength() + "'"); }
		 */
		
		System.out.println();
	}

	/**
	 * Compare CDs by number of tracks
	 * If 2 CDs have the same amount of tracks, compare by their length
	 */
	@Override
	public int compareTo(CompactDisc cd) {
		// TODO Auto-generated method stub
		if (this.tracks.size() > cd.tracks.size())
			return 1;
		else if (this.tracks.size() < cd.tracks.size())
			return -1;
		else {
			if (this.getLength() > cd.getLength())
				return 1;
			else if (this.getLength() < cd.getLength())
				return -1;
			else 
				return 0;
		}
	}

}
