package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class BookTest {

	public static void main(String[] args) {
		
		List<String> authors = new ArrayList<>();
		authors.add("Robert Kiyosaki");
		authors.add("Sharon Lechter");
		
		Book book = new Book("Rich Dad Poor Dad", "Business", 3.45f, authors);
		book.setContent("Rich Dad Poor Dad is a 1997 book, written by Robert Kiyosaki & Sharon Lechter");
		
		System.out.println(book.toString());
	}

}
