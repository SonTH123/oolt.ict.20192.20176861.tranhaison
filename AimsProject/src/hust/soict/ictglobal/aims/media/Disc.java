package hust.soict.ictglobal.aims.media;

public class Disc extends Media{

	private float length;
	private String director;
	
	public Disc(String title) {
		super(title);
	}
	
	public Disc(String title, String category) {
		super(title, category);
	}
	
	public Disc(String title, String category, float cost) {
		super(title, category, cost);
	}
	
	public Disc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}
	
	public Disc(float length, String director) {
		super();
		this.length = length;
		this.director = director;
	}
	
	public Disc(String title, float length, String director) {
		super(title);
		this.length = length;
		this.director = director;
	}
	
	public Disc(String title, String category, float length, String director) {
		super(title, category);
		this.length = length;
		this.director = director;
	}
	
	public Disc(String title, String category, float cost, float length, String director) {
		super(title, category, cost);
		this.length = length;
		this.director = director;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

}
