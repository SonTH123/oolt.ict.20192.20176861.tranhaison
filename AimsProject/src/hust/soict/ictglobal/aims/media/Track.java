package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public class Track implements Playable, Comparable<Track> {

	private String title;
	private float length;
	
	public Track(String title, float length) {
		super();
		this.title = title;
		this.length = length;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	@Override
	public void play() throws PlayerException {
		if (this.length <= 0) {
			System.err.println("ERROR: Track length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing track: " + title);
		System.out.println("Track length: " + length);
	}

	/**
	 * Compare tracks by their title
	 */
	@Override
	public int compareTo(Track track) {
		// TODO Auto-generated method stub
		return this.getTitle().compareTo(track.getTitle());
	}

}
