// Exercise 7: Add 2 matrices of the same size
package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Exercise7 {
	private static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		int m, n;
		int i, j;

	    System.out.println("Enter the number of rows and columns of matrix");
	    m = scan.nextInt();
	    n = scan.nextInt();

	    int first[][] = new int[m][n];
	    int second[][] = new int[m][n];
	    int sum[][] = new int[m][n];

	    System.out.println("Enter the elements of first matrix");

	    for (i = 0; i < m; i++)
	      for (j = 0; j < n; j++)
	        first[i][j] = scan.nextInt();

	    System.out.println("Enter the elements of second matrix");

	    for (i = 0 ; i < m; i++)
	      for (j = 0 ; j < n; j++)
	        second[i][j] = scan.nextInt();

	    for (i = 0; i < m; i++)
	      for (j = 0; j < n; j++)
	        sum[i][j] = first[i][j] + second[i][j]; 

	    System.out.println("The new matrices is:");

	    for (i = 0; i < m; i++) {
	    	for (j = 0; j < n; j++)
		        System.out.print(sum[i][j] + "\t");
	    	System.out.println();
	    }
	}

}
