// Exercise 4: Write a program to display a triangle with a height of n stars (*), n is entered by user
/* Example: n=5
 *     *
 *    ***
 *   *****
 *  *******
 * *********
 */

package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Exercise4 {
	
	private static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		int number;
		do {
			System.out.print("Enter a number: ");
			number = scan.nextInt();
		} while (number < 1);
		
		int i,  j;
		for (i=1; i<=number; i++) {
			// Firstly is to print the blank space
			int space = number - i;
			for (j=1; j<=space; j++) {
				System.out.print(" ");
			}
			// Secondly is to print the number of asterisks
			int numberofAsterisk = 2*i - 1;
			for (j=1; j<=numberofAsterisk; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}
