// Exercise 2: Program for input and output from keyboard
package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Exercise2 {
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("What is your name? ");
		String name = scan.nextLine();
		System.out.println("How old are you? ");
		int age = scan.nextInt();
		System.out.println("How tall are you (m)? ");
		double height = scan.nextDouble();
		
		// similar to other data types
		// nextByte(), nextShort(), nextLong()
		// nextFloat(), nextBoolean()
		
		System.out.println("Mr/Mrs " + name + ", " + age + " years old." + " Your height is: " + height + " m.");
	}

}
