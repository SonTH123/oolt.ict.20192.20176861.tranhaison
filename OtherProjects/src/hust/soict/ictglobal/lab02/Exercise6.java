// Exercise 6: Write a Java program to sort a numeric array, and calculate the sum and average value of array elements.
package hust.soict.ictglobal.lab02;

import java.util.Scanner;
import java.util.ArrayList;

public class Exercise6 {
	private static Scanner scan = new Scanner(System.in);
	private static ArrayList<Integer> array = new ArrayList<Integer>();
	
	public static void main(String[] args) {
		System.out.print("Enter the number of elements: ");
		int num = scan.nextInt();
		
		for (int i=0; i<num; i++) {
			System.out.printf("Enter number %d: ", i+1);
			int temp = scan.nextInt();
			array.add(temp);
		}
		
		int sum = sumofArray();
		double avg = averageofArray();
		
		System.out.print("The array before sorting is: ");
		for (int element: array)
			System.out.print(element + " ");
		System.out.println();
		
		quickSort(0, array.size() - 1);
		
		System.out.print("The array after sorting is: ");
		for (int element: array)
			System.out.print(element + " ");
		System.out.println();
		
		System.out.println("The sum of all elements is: " + sum 
				  + "\nThe average of all elements is: " + avg);
	}
	
	public static int sumofArray() {
		int sum = 0;
		for (int element: array)
			sum += element;
		return sum;
	}

	public static double averageofArray() {

		double avg;
		avg = sumofArray() / array.size();
		return avg;
	}

	public static int partition (int low, int high) {
		int pivot = array.get(high);
		int left = low;
		int right = high-1;
		int temp;
		
		while (true) {
			while ((left <= right) && (array.get(left) < pivot))
				left++;
			while ((right >= left) && (array.get(right) > pivot))
				right--;
			
			if (left > right)	
				break;
			
			temp = array.get(left);
			array.set(left, array.get(right));
			array.set(right, temp);
			
			left++;
			right--;
		}
		
		temp = array.get(left);
		array.set(left, array.get(high));
		array.set(high, temp);
		
		return left;
	}
	
	public static void quickSort (int low, int high) {
		if (low < high) {
			int pi = partition(low, high);
			quickSort(low, pi - 1);
			quickSort(pi + 1, high);
		}
	}
}
