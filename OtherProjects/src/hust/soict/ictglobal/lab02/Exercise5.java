// Exercise 5: Write a program to display the number of days of a month
package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Exercise5 {
	public static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		int month, year=0;
		do {
			System.out.print("Enter the year (number): ");
			year = scan.nextInt();
		} while (year <= 0);
		do {
			System.out.print("Enter the month (number): ");
			month = scan.nextInt();
		} while (month > 12 || month < 1);
		
		int day = numberofDaysinMonth(month, year);
		System.out.println("There are " + day + " days");
	}
	
	public static boolean isLeapYear(int year) {
		if (year % 400 == 0)
			return true;
		if ((year % 4 == 0) && (year % 100 != 0))
			return true;
		return false;
	}
	
	public static int numberofDaysinMonth(int month, int year) {
		switch(month) {
			case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
				return 31;
			}
			case 4: case 6: case 9: case 11: {
				return 30;
			}
			case 2: {
				if (isLeapYear(year) == true)
					return 29;
				return 28;
			}
		}
		return 0;
	}
}
