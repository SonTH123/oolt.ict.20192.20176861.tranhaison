// Write, compile and run the ChoosingOption program
package hust.soict.ictglobal.lab02;

import javax.swing.JOptionPane;

public class Exercise1 {

	public static void main(String[] args) {
		int option = JOptionPane.showConfirmDialog(null, "Do you want to change to the first class ticket?");
		JOptionPane.showMessageDialog(null, "You have chosen: " + (option == JOptionPane.YES_OPTION?"Yes":"No"));
		System.exit(0);
	}
}
