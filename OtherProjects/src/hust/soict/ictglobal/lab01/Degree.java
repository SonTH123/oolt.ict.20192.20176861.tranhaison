// Example 6
package hust.soict.ictglobal.lab01;

import javax.swing.JOptionPane;

public class Degree {
	
	final static double INVALID_VALUE = Double.POSITIVE_INFINITY;
	
	public static void main(String[] args) {
		String input_choice;
		int choice;
		
		do {
			input_choice = JOptionPane.showInputDialog(null, "What do you want to do?"
				+ "\n1. First degree 1 variable"
				+ "\n2. First degree 2 variable"
				+ "\n3. Second degree 1 variable"
				+ "\n4. Exit");
			choice = Integer.parseInt(input_choice);
		} while (choice < 1 || choice > 4);
		
		switch (choice) 
		{
		case 1: {
			firstDegreeOneVariable();
			break;
		}
		case 2:
		{
			firstDegreeTwoVariables();
			break;
		}
		case 3:
		{
			secondDegree();
			break;
		}
		case 4: 
			JOptionPane.showMessageDialog(null, "Bye bye!");
			break;
		}

		System.exit(0);
	}
	
	// First degree with 1 variable
	public static void firstDegreeOneVariable () {
		String s1, s2;
		double a, b;
		
		s1 = JOptionPane.showInputDialog(null, "Enter a");
		a = Double.parseDouble(s1);
		s2 = JOptionPane.showInputDialog(null, "Enter b");
		b = Double.parseDouble(s2);
		
		if (a == 0 && b != 0)
			JOptionPane.showMessageDialog(null, "INVALID_VALUE");
		else if (a == 0 && b == 0)
			JOptionPane.showMessageDialog(null, "INFINITE VALUE FOR EVERY X");
		else {
			double x;
			x = - (b/a);	
			JOptionPane.showMessageDialog(null, "x = " + x);
		}
	}
	
	// First degree with 2 variables
	public static void firstDegreeTwoVariables() {
		String s1, s2, s3, s4, s5, s6;
		double a, b, c, d, e, f;
		
		s1 = JOptionPane.showInputDialog(null, "Enter a");
		a = Double.parseDouble(s1);
		s2 = JOptionPane.showInputDialog(null, "Enter b");
		b = Double.parseDouble(s2);
		s3 = JOptionPane.showInputDialog(null, "Enter c");
		c = Double.parseDouble(s3);
		s4 = JOptionPane.showInputDialog(null, "Enter d");
		d = Double.parseDouble(s4);
		s5 = JOptionPane.showInputDialog(null, "Enter e");
		e = Double.parseDouble(s5);
		s6 = JOptionPane.showInputDialog(null, "Enter f");
		f = Double.parseDouble(s6);
		
		if ((a*d) - (b*c) != 0) {
			double x = ((e*d) - (b*f)) / ((a*d) - (b*c));
			double y = ((a*f) - (e*c)) / ((a*d) - (b*c));
			JOptionPane.showMessageDialog(null, "x = " + x + " and y = " + y);
		}
		else if (a == b && b == c && c == d && d == e & e == f)
			JOptionPane.showMessageDialog(null, "INFINITE VALUE FOR X AND Y");
		else {
			JOptionPane.showMessageDialog(null, "INVALID_VALUE");
		}
	}
	
	// Second degree with 1 variable
	public static void secondDegree () {
		String s1, s2, s3;
		double a, b, c;
		
		s1 = JOptionPane.showInputDialog(null, "Enter a");
		a = Double.parseDouble(s1);
		s2 = JOptionPane.showInputDialog(null, "Enter b");
		b = Double.parseDouble(s2);
		s3 = JOptionPane.showInputDialog(null, "Enter c");
		c = Double.parseDouble(s3);
		
		double delta = b * b - 4.0 * a * c;
		double r;
		
		if (a == 0 && b == 0 && c == 0)
			JOptionPane.showMessageDialog(null, "INFINITE_VALUE");
		else if (a == 0 && b == 0 && c != 0)
			JOptionPane.showMessageDialog(null, "INVALID_VALUE");
		else if (a == 0 && b != 0) {
			r = (-c)/b;
			JOptionPane.showMessageDialog(null, "x = " + r);
		}
		else {
			if (delta > 0.0) {
				double r1 = (-b + Math.pow(delta, 0.5)) / (2.0 * a);
				double r2 = (-b - Math.pow(delta, 0.5)) / (2.0 * a);
				JOptionPane.showMessageDialog(null, "x1 = " + r1 + " and x2 = " + r2);
			} else if (delta == 0.0) {
				r = -b / (2.0 * a);
				JOptionPane.showMessageDialog(null, "x = " + r);
			} else {
				JOptionPane.showMessageDialog(null, "INVALID_VALUE");
			}
		}

	}

}