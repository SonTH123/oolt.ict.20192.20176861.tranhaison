// Example 4: ShowTwoNumbers.java
package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;
public class ShowTwoNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1, s2;
		String noti = "You have just entered: ";
		
		s1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number",
				JOptionPane.INFORMATION_MESSAGE);
		noti += s1 + " and ";
		
		s2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number",
				JOptionPane.INFORMATION_MESSAGE);
		
		noti += s2;
		
		JOptionPane.showMessageDialog(null, noti, "Show 2 numbers", JOptionPane.INFORMATION_MESSAGE);
		
		System.exit(0);
	}

}