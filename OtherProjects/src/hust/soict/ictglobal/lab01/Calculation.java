//Exercise 5: Calculate sum, difference, product and quotient of 2 double numbers
package hust.soict.ictglobal.lab01;

import javax.swing.JOptionPane;

public class Calculation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1, s2;
		
		s1 = JOptionPane.showInputDialog(null, "Please input first number", "Input first number", 
				JOptionPane.INFORMATION_MESSAGE);
		double num1 = Double.parseDouble(s1);
		
		s2 = JOptionPane.showInputDialog(null, "Please input second number", "Input second number", 
				JOptionPane.INFORMATION_MESSAGE);
		double num2 = Double.parseDouble(s2);
		
		double sum, product, difference, quotient;
		sum = num1 + num2;
		product = num1 * num2;
		difference = num1 - num2;
		
		if (num2 == 0)
			JOptionPane.showMessageDialog(null, 
					   "The sum is: " + sum 
					+ "\n The product is: " + product
					+ "\n The difference is: " + difference
					+ "\n The quotient is: INVALID_VALUE");
		else {
			quotient = num1/num2;
			JOptionPane.showMessageDialog(null, 
					   "The sum is: " + sum 
					+ "\n The product is: " + product
					+ "\n The difference is: " + difference
					+ "\n The quotient is: " + quotient);
		}
		
		System.exit(0);
	}

}