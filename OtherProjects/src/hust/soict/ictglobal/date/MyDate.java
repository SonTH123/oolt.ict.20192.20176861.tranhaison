package hust.soict.ictglobal.date;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDate {
	private String day;
	private String month;
	private int year;
	
	// Get the current date and pass to 3 attributes
	public MyDate() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String currDate = sdf.format(date);
		
		String[] splitDate = currDate.split("/");
		this.day = splitDate[0];
		this.month = splitDate[1];
		this.year = Integer.parseInt(splitDate[2]);
	}
	
	// Get the date as user's input for each attribute
	public MyDate(String month, String day, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	// Get a string and split into 3 attributes day, month, year
	public MyDate(String date) {
		String[] splitDate = date.split(" ");
		this.month = splitDate[0];
		this.day = splitDate[1];
		this.year = Integer.parseInt(splitDate[2]);
	}
	
	// Getter & Setter
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public void setMonth(int month) {
		this.month = String.valueOf(month);
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void setYear(String year) {
		this.year = Integer.parseInt(year);
	}
	
	public boolean accept(String date) {
		// Get the date from user's input and pass to 3 attributes
		MyDate currDate = new MyDate(date);
		
		if (currDate.getYear() <= 0)	
			return false;
		
		// Convert the month into number format display
		switch (currDate.getMonth()) {
			case "Jan": case "January": case "1": {
				currDate.setMonth("01");
				break;
			}
			case "Feb": case "February": case "2": {
				currDate.setMonth("02");
				break;
			}
			case "Mar": case "March": case "3": {
				currDate.setMonth("03");
				break;
			}
			case "Apr": case "April": case "4": {
				currDate.setMonth("04");
				break;
			}
			case "May": case "5": {
				currDate.setMonth("05");
				break;
			}
			case "June": case "6": {
				currDate.setMonth("06");
				break;
			}
			case "July": case "7": {
				currDate.setMonth("07");
				break;
			}
			case "Aug": case "August": case "8": {
				currDate.setMonth("08");
				break;
			}
			case "Sep": case "September": case "9": {
				currDate.setMonth("09");
				break;
			}
			case "Oct": case "October": case "10": {
				currDate.setMonth("10");
				break;
			}
			case "Nov": case "November": case "11": {
				currDate.setMonth("11");
				break;
			}
			case "Dec": case "December": case "12": {
				currDate.setMonth("12");
				break;
			}
			default:
				return false;
		}
		
		// Check if the day is valid or not
		int today = Integer.parseInt(currDate.getDay());
		if (today <= 0 || today > numberofDaysinMonth(currDate.getMonth(), currDate.getYear())) 
			return false;

		return true;
	}
	
	public void convertMonth(MyDate currDate) {
	// Convert the month into number format display
			switch (currDate.getMonth()) {
				case "Jan": case "January": case "1": {
					currDate.setMonth("01");
					break;
				}
				case "Feb": case "February": case "2": {
					currDate.setMonth("02");
					break;
				}
				case "Mar": case "March": case "3": {
					currDate.setMonth("03");
					break;
				}
				case "Apr": case "April": case "4": {
					currDate.setMonth("04");
					break;
				}
				case "May": case "5": {
					currDate.setMonth("05");
					break;
				}
				case "June": case "6": {
					currDate.setMonth("06");
					break;
				}
				case "July": case "7": {
					currDate.setMonth("07");
					break;
				}
				case "Aug": case "August": case "8": {
					currDate.setMonth("08");
					break;
				}
				case "Sep": case "September": case "9": {
					currDate.setMonth("09");
					break;
				}
				case "Oct": case "October": case "10": {
					currDate.setMonth("10");
					break;
				}
				case "Nov": case "November": case "11": {
					currDate.setMonth("11");
					break;
				}
				case "Dec": case "December": case "12": {
					currDate.setMonth("12");
					break;
				}
			}
	}
	
	private boolean isLeapYear(int year) {
		if (year % 400 == 0)
			return true;
		if ((year % 4 == 0) && (year % 100 != 0))
			return true;
		return false;
	}
	
	private int numberofDaysinMonth(String month, int year) {
		switch(month) {
			case "01": case "03": case "05": case "07": case "08": case "10": case "12": {
				return 31;
			}
			case "04": case "06": case "09": case "11": {
				return 30;
			}
			case "02": {
				if (isLeapYear(year) == true)
					return 29;
				return 28;
			}
		}
		return 0;
	}
	
	public void printDate() {
		System.out.println("The date is: " + day + " " + month + " " + year);
	}
	
	public void printDate(MyDate currDate) {
		convertMonth(currDate);
		System.out.println("The date is: " + currDate.day + "/" + currDate.month + "/" + currDate.year);
	}
}
