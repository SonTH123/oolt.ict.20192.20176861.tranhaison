package hust.soict.ictglobal.date;

import java.util.Scanner;
import java.util.ArrayList;

public class DateTest {
	
	private static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// create a new instance which is a current date
		MyDate date1 = new MyDate();
		date1.printDate(date1);
		
		// create a new instance in which user must input 3 parameters month, day and year
		MyDate date2 = new MyDate("Jan", "12", 2000);
		date2.printDate();
		
		// create a new instance in which user must input a string containing month, day, year and it must have a space between each
		MyDate date3 = new MyDate("February 29 2020");
		date3.printDate();
		
		// Ask user to enter a date in format Month Day Year (Month can be a string or a number)
		System.out.println("Enter the date (Month Day Year): ");
		String inputDate = scan.nextLine();
		MyDate date5 = new MyDate(inputDate);
		boolean isValid = date5.accept(inputDate);
		if (isValid) {
			date5.printDate(date5);
			System.out.println();
		} else {
			System.out.println("The date is wrong!\n");
		}
		
		// Create new instances date6, date7 and compare them and print to see which one occur before or after other
		MyDate date6 = new MyDate("March 31 2020");
		MyDate date7 = new MyDate("June", "31", 2020);
		System.out.println("The date 1 is: " + date6.getDay() + " - " + date6.getMonth() + " - " + date6.getYear());
		System.out.println("The date 2 is: " + date7.getDay() + " - " + date7.getMonth() + " - " + date7.getYear());
		int compare = DateUtils.compareDate(date6,  date7);
		if (compare == -1)
			System.out.println("Date 1 occurs before Date 2\n");
		else if (compare == 1)	
			System.out.println("Date 1 occurs after Date 2\n");
		else if (compare == 0)
			System.out.println("Date 1 and Date 2 are the same\n");
		else
			System.out.println("WRONG\n");
		
		// Create new date instances, add to an array list to sort and print the list of date in ascending order
		MyDate date8 = new MyDate("March 31 2020");
		MyDate date9 = new MyDate("June", "31", 2019);
		MyDate date10 = new MyDate("July", "05", 2019);
		MyDate date11 = new MyDate("Sep", "12", 2021);
		
		ArrayList<MyDate> dateList = new ArrayList<>();
		dateList.add(date8);
		dateList.add(date9);
		dateList.add(date10);
		dateList.add(date11);
		
		System.out.println("The list of dates before sorting is: ");
		for (int i=0; i<dateList.size(); i++) {
			System.out.println("Date " + (i+1) + " is: " 
					+ dateList.get(i).getDay() + " - " 
					+ dateList.get(i).getMonth() + " - "
					+ dateList.get(i).getYear());
		}
		
		DateUtils.sortDate(dateList);	
		System.out.println("\nThe list of dates after sorting is: ");
		for (int i=0; i<dateList.size(); i++) {
			System.out.println("Date " + (i+1) + " is: " 
					+ dateList.get(i).getDay() + " - " 
					+ dateList.get(i).getMonth() + " - "
					+ dateList.get(i).getYear());
		}
		
	}

}
