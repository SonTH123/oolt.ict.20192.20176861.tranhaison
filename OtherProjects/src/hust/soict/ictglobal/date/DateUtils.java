package hust.soict.ictglobal.date;

import java.util.ArrayList;

public class DateUtils {
	
	// Compare 2 dates and return 
	// -1 if Date 1 < Date 2
	// 0 if Date 1 = Date 2
	// 1 if Date 1 > Date 2
	public static int compareDate(MyDate date1, MyDate date2) {
		if (date1.getYear() < date2.getYear())
			return -1;
		else if (date1.getYear() > date2.getYear())
			return 1;
		else {
			date1.convertMonth(date1);
			date2.convertMonth(date2);
			int month1 = Integer.parseInt(date1.getMonth());
			int month2 = Integer.parseInt(date2.getMonth());
			
			if (month1 < month2)
				return -1;
			else if (month1 > month2)
				return 1;
			else {
				int day1 = Integer.parseInt(date1.getDay());
				int day2 = Integer.parseInt(date2.getDay());
				
				if (day1 < day2)
					return -1;
				else if (day1 > day2)
					return 1;
				else
					return 0;
			}
		}
	}
	
	// Sort a number of date with Bubble Sort algorithm
	public static void sortDate(ArrayList<MyDate> dateList) {
		for (int i=0; i< dateList.size(); i++) {
			for (int j=i+1; j<dateList.size(); j++) {
				if (compareDate(dateList.get(i), dateList.get(j)) == 1) {
					MyDate temp = dateList.get(i);
					dateList.set(i, dateList.get(j));
					dateList.set(j, temp);
				}
			}
		}
	}
}
