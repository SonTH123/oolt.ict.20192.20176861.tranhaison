package hust.soict.ictglobal.garbage;

import java.io.IOException;
import java.io.BufferedReader; 
import java.io.FileReader; 

public class GarbageCreator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String string = "";	
		// set start as the time when this block of code start running
		long start = System.currentTimeMillis();
		BufferedReader br = null;
		try {
			FileReader fr = new FileReader("filename.txt"); 
			br = new BufferedReader(fr);
			
			// read character by character and concatenate to String until the end of the text
			int num = 0;
			while ((num = br.read()) != -1) {
				string += (char) num;
			}
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println(string + "\n");
		System.out.print("The time to read a file to a String is: ");
		System.out.println(System.currentTimeMillis() - start);
		
	}

}
