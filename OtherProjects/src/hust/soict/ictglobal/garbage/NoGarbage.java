package hust.soict.ictglobal.garbage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class NoGarbage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		StringBuilder sb = new StringBuilder();
		long start = System.currentTimeMillis();
		BufferedReader br = null;
		try {
			FileReader fr2 = new FileReader("filename.txt"); 
			br = new BufferedReader(fr2);
			
			// read character by character and concatenate to StringBuffered until the end of the text
			int num2 = 0;
			while ((num2 = br.read()) != -1) {
				sb.append((char) num2);
			}
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println(sb + "\n");
		System.out.print("The time to read a file to a StringBuffer is: ");
		System.out.println(System.currentTimeMillis() - start);
	}

}
