package hust.soict.ictglobal.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class AWTAccumulator extends Frame implements ActionListener{

	private Label lblInput;       // Declare input Label
	private Label lblOutput;     // Declare output Label
	private TextField tfInput;   // Declare input TextField
	private TextField tfOutput; // Declare output TextField
	private int sum = 0;           // Accumulated sum, init to 0
	   
	// Constructor to setup the GUI components and event handlers
	public AWTAccumulator() {
		setLayout(new FlowLayout());
		// "super" Frame (container) sets layout to FlowLayout, which arranges
        // the components from left-to-right, and flow to next row from top-to-bottom.
		
	    lblInput = new Label("Enter an Integer: ");
	    add(lblInput);     
	    
	    tfInput = new TextField(10); 
	    add(tfInput); 
	 
	    tfInput.addActionListener(this);
	    // "tfInput" is the source object that fires an ActionEvent upon entered.
        // The source add "this" instance as an ActionEvent listener, which provides
        //  an ActionEvent handler called actionPerformed().
        // Hitting "enter" on tfInput invokes actionPerformed().
	    
	    lblOutput = new Label("The Accumulated Sum is: ");
	    add(lblOutput);  
	    
	    tfOutput = new TextField(10);
	    tfOutput.setEditable(false);
	    add(tfOutput);
	    
	    setTitle("AWT Accumulator");  // "super" Frame sets title
	    setSize(350, 120);  // "super" Frame sets initial window size
	    setVisible(true);    // "super" Frame shows
	}

	public static void main(String[] args) {
		new AWTAccumulator();
	}

	// ActionEvent handler - Called back upon hitting "enter" key on TextField
	@Override
	public void actionPerformed(ActionEvent e) {
		// Get the String entered into the TextField tfInput, convert to integer
	    int numberIn = Integer.parseInt(tfInput.getText());
	    sum += numberIn;      // Accumulate numbers entered into sum
	    tfInput.setText("");     // Clear input TextField
	    tfOutput.setText(String.valueOf(sum));  // Display sum on the output TextField
	    											// convert int to String
	}

}
