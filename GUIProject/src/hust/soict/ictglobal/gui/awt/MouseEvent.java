package hust.soict.ictglobal.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class MouseEvent extends Frame implements MouseListener{

	private TextField tfMouseX; // to display mouse-click-x
	private TextField tfMouseY; // to display mouse-click-y
	
	// Constructor - Setup the UI components and event handlers
	public MouseEvent() {
		setLayout(new FlowLayout()); // "super" frame sets its layout to FlowLayout
		
		// Label (anonymous)
	    add(new Label("X-Click: ")); // "super" frame adds Label component
	    
	    // TextField
	    tfMouseX = new TextField(10); // 10 columns
	    tfMouseX.setEditable(false);  	// read-only
	    add(tfMouseX);                			// "super" frame adds TextField component
	    
	    // Label (anonymous)
	    add(new Label("Y-Click: ")); 	// "super" frame adds Label component
	    
	    // TextField
	    tfMouseY = new TextField(10);	// 10 columns
	    tfMouseY.setEditable(false);  	// read-only
	    add(tfMouseY);                			// "super" frame adds TextField component
	    
	    addMouseListener(this);
        // "super" frame (source) fires the MouseEvent.
        // "super" frame adds "this" object as a MouseEvent listener.
	    
	    setTitle("MouseEvent Demo"); 	// "super" Frame sets title
	    setSize(350, 100);           			// "super" Frame sets initial size
	    setVisible(true);            			// "super" Frame shows
	}

	public static void main(String[] args) {
		new MouseEvent();
	}

	/* MouseEvent handlers */
	// Called back upon mouse clicked
	@Override
	public void mouseClicked(java.awt.event.MouseEvent e) {
		tfMouseX.setText(e.getX() + "");
	    tfMouseY.setText(e.getY() + "");
	}

	@Override public void mousePressed(java.awt.event.MouseEvent e) {}
	@Override public void mouseReleased(java.awt.event.MouseEvent e) {}
	@Override public void mouseEntered(java.awt.event.MouseEvent e) {}
	@Override public void mouseExited(java.awt.event.MouseEvent e) {}

}
