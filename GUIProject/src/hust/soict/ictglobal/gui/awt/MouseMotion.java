package hust.soict.ictglobal.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class MouseMotion extends Frame implements MouseListener, MouseMotionListener{

	// To display the (x, y) of the mouse-clicked
	private TextField tfMouseClickX;
	private TextField tfMouseClickY;
	// To display the (x, y) of the current mouse-pointer position
	private TextField tfMousePositionX;
	private TextField tfMousePositionY;
	   
	public MouseMotion() {
		setLayout(new FlowLayout()); // "super" frame sets to FlowLayout
		 
	    add(new Label("X-Click: "));
	    tfMouseClickX = new TextField(10);
	    tfMouseClickX.setEditable(false);
	    add(tfMouseClickX);
	    
	    add(new Label("Y-Click: "));
	    tfMouseClickY = new TextField(10);
	    tfMouseClickY.setEditable(false);
	    add(tfMouseClickY);
	 
	    add(new Label("X-Position: "));
	    tfMousePositionX = new TextField(10);
	    tfMousePositionX.setEditable(false);
	    add(tfMousePositionX);
	    
	    add(new Label("Y-Position: "));
	    tfMousePositionY = new TextField(10);
	    tfMousePositionY.setEditable(false);
	    add(tfMousePositionY);
	    
	    addMouseListener(this);
	    addMouseMotionListener(this);
	    // "super" frame (source) fires MouseEvent.
	    // "super" frame adds "this" object as MouseListener and MouseMotionListener.
	 
	    setTitle("MouseMotion Demo"); // "super" Frame sets title
	    setSize(400, 120);            // "super" Frame sets initial size
	    setVisible(true);             // "super" Frame shows
	}

	public static void main(String[] args) {
		new MouseMotion();
	}
	
	/** MouseListener handlers */
	// Called back when a mouse-button has been clicked
	@Override
	public void mouseClicked(java.awt.event.MouseEvent evt) {
	      tfMouseClickX.setText(evt.getX() + "");
	      tfMouseClickY.setText(evt.getY() + "");
	}
	 
	// Not Used, but need to provide an empty body for compilation
	@Override public void mousePressed(java.awt.event.MouseEvent e) {}
	@Override public void mouseReleased(java.awt.event.MouseEvent e) {}
	@Override public void mouseEntered(java.awt.event.MouseEvent e) {}
	@Override public void mouseExited(java.awt.event.MouseEvent e) {}
	
	/** MouseMotionEvent handlers */
	// Called back when the mouse-pointer has been moved
	@Override
	public void mouseMoved(java.awt.event.MouseEvent evt) {
	     tfMousePositionX.setText(evt.getX() + "");
	     tfMousePositionY.setText(evt.getY() + "");
	 }
	 
	 // Not Used, but need to provide an empty body for compilation
	@Override public void mouseDragged(java.awt.event.MouseEvent evt) { }

}
